

#include "stm32f401_discovery.h"
#include "FreeRTOS.h"
#define __STM32F401_DISCO_BSP_VERSION_MAIN   (0x02) /*!< [31:24] main version */
#define __STM32F401_DISCO_BSP_VERSION_SUB1   (0x00) /*!< [23:16] sub1 version */
#define __STM32F401_DISCO_BSP_VERSION_SUB2   (0x00) /*!< [15:8]  sub2 version */
#define __STM32F401_DISCO_BSP_VERSION_RC     (0x00) /*!< [7:0]  release candidate */ 
#define __STM32F401_DISCO_BSP_VERSION         ((__STM32F401_DISCO_BSP_VERSION_MAIN << 24)\
                                             |(__STM32F401_DISCO_BSP_VERSION_SUB1 << 16)\
                                             |(__STM32F401_DISCO_BSP_VERSION_SUB2 << 8 )\
                                             |(__STM32F401_DISCO_BSP_VERSION_RC))  

char debug_buf[1000] = "";

GPIO_TypeDef* GPIO_PORT[LEDn] = {LED4_GPIO_PORT, 
                                 LED3_GPIO_PORT, 
                                 LED5_GPIO_PORT,
                                 LED6_GPIO_PORT};
const uint16_t GPIO_PIN[LEDn] = {LED4_PIN, 
                                 LED3_PIN, 
                                 LED5_PIN,
                                 LED6_PIN};

GPIO_TypeDef* BUTTON_PORT[BUTTONn] = {KEY_BUTTON_GPIO_PORT}; 
const uint16_t BUTTON_PIN[BUTTONn] = {KEY_BUTTON_PIN};
const uint8_t BUTTON_IRQn[BUTTONn] = {KEY_BUTTON_EXTI_IRQn};

void I2Cx_Init(void){
        I2C_DeInit(I2C1);
    	GPIO_InitTypeDef GPIO_InitStruct;
    	I2C_InitTypeDef I2C_InitStruct;
    
    	// enable APB1 peripheral clock for I2C1
    	RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
    	// enable clock for SCL and SDA pins
    	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
    
    	/* setup SCL and SDA pins
    	 * You can connect the I2C1 functions to two different
    	 * pins:
    	 * 1. SCL on PB6 or PB8  
    	 * 2. SDA on PB7 or PB9
    	 */
    	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_9; // we are going to use PB6 and PB9
    	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;			// set pins to alternate function
    	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;		// set GPIO speed
    	GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;			// set output to open drain --> the line has to be only pulled low, not driven high
    	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;			// enable pull up resistors
    	GPIO_Init(GPIOB, &GPIO_InitStruct);					// init GPIOB
    
    	// Connect I2C1 pins to AF  
    	GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_I2C1);	// SCL
    	GPIO_PinAFConfig(GPIOB, GPIO_PinSource9, GPIO_AF_I2C1); // SDA
    
    	// configure I2C1 
    	I2C_InitStruct.I2C_ClockSpeed = 100000; 		// 10kHz
    	I2C_InitStruct.I2C_Mode = I2C_Mode_I2C;			// I2C mode
    	I2C_InitStruct.I2C_DutyCycle = I2C_DutyCycle_2;	// 50% duty cycle --> standard
    	I2C_InitStruct.I2C_OwnAddress1 = 0x43;			// own address, not relevant in master mode
    	I2C_InitStruct.I2C_Ack = I2C_Ack_Enable;		// disable acknowledge when reading (can be changed later on)
    	I2C_InitStruct.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit; // set address length to 7 bit addresses
        I2C_DeInit(I2C1);
    	I2C_Init(I2C1, &I2C_InitStruct);				// init I2C1
    
    	// enable I2C1
    	I2C_Cmd(I2C1, ENABLE);
}

/* This function issues a start condition and 
 * transmits the slave address + R/W bit
 * 
 * Parameters:
 * 		I2Cx --> the I2C peripheral e.g. I2C1
 * 		address --> the 7 bit slave address
 * 		direction --> the transmission direction can be:
 * 						I2C_Direction_Tranmitter for Master transmitter mode
 * 						I2C_Direction_Receiver for Master receiver
 */
void I2Cx_start(I2C_TypeDef* I2Cx, uint8_t address, uint8_t direction){
	// wait until I2C1 is not busy any more
	while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_BUSY));
  
	// Send I2C1 START condition 
	I2C_GenerateSTART(I2Cx, ENABLE);

	// wait for I2C1 EV5 --> Slave has acknowledged start condition
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_MODE_SELECT));

	// Send slave Address for write 
	I2C_Send7bitAddress(I2Cx, address, direction);

	/* wait for I2Cx EV6, check if 
	 * either Slave has acknowledged Master transmitter or
	 * Master receiver mode, depending on the transmission
	 * direction
	 */ 
	if(direction == I2C_Direction_Transmitter){
        while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED));
	}
	else if(direction == I2C_Direction_Receiver){
        while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED));
	}
    
}

/* This function transmits one byte to the slave device
 * Parameters:
 *		I2Cx --> the I2C peripheral e.g. I2C1 
 *		data --> the data byte to be transmitted
 */
void I2Cx_write(I2C_TypeDef* I2Cx, uint8_t data)
{
	// wait for I2C1 EV8 --> last byte is still being transmitted (last byte in SR, buffer empty), next byte can already be written
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTING));
	I2C_SendData(I2Cx, data);
    while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED));
}

/* This function reads one byte from the slave device 
 * and acknowledges the byte (requests another byte)
 */
uint8_t I2Cx_read_ack(I2C_TypeDef* I2Cx){
	// enable acknowledge of received data
	I2C_AcknowledgeConfig(I2Cx, ENABLE);
	// wait until one byte has been received
	while( !I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_RECEIVED) );
	// read data from I2C data register and return data byte
	uint8_t data = I2C_ReceiveData(I2Cx);
	return data;
}

/* This function reads one byte from the slave device
 * and doesn't acknowledge the received data 
 * after that a STOP condition is transmitted
 */
uint8_t I2Cx_read_nack(I2C_TypeDef* I2Cx){
	// disable acknowledge of received data
	// nack also generates stop condition after last byte received
	// see reference manual for more info
	I2C_AcknowledgeConfig(I2Cx, DISABLE);
	while( !I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_RECEIVED) );
	I2C_GenerateSTOP(I2Cx, ENABLE);
	// wait until one byte has been received
	// read data from I2C data register and return data byte
	uint8_t data = I2C_ReceiveData(I2Cx);
	return data;
}

/* This function issues a stop condition and therefore
 * releases the bus
 */
void I2Cx_stop(I2C_TypeDef* I2Cx){

	// Send I2C1 STOP Condition after last byte has been transmitted
	I2C_GenerateSTOP(I2Cx, ENABLE);
}

void I2Cx_WriteData(uint16_t Addr, uint8_t Reg, uint8_t Value)
{
    I2Cx_start(I2C1, Addr, I2C_Direction_Transmitter);
    I2Cx_write(I2C1, Reg);
    I2Cx_write(I2C1, Value);
    I2Cx_stop(I2C1);
}

uint8_t I2Cx_ReadData(uint16_t Addr, uint8_t Reg)
{
    uint8_t data_read;
    I2Cx_start(I2C1, Addr, I2C_Direction_Transmitter);
    I2Cx_write(I2C1, Reg);
    I2Cx_stop(I2C1);
    I2Cx_start(I2C1, Addr, I2C_Direction_Receiver);
    data_read = I2Cx_read_nack(I2C1);
    return data_read;
}


void SPIx_Init(void)
{
    SPI_DeInit(SPI1);
    GPIO_InitTypeDef GPIO_InitStruct;
    SPI_InitTypeDef SPI_InitStruct;

    // enable APB2 peripheral clock for SPI1
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);
    // enable clock for SPI pins
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

    GPIO_InitStruct.GPIO_Pin   = GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7;
    GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_AF;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStruct.GPIO_PuPd  = GPIO_PuPd_NOPULL;
    GPIO_Init(GPIOA, &GPIO_InitStruct);

    // connect SPI1 pins to AF
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource5, GPIO_AF_SPI1);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource6, GPIO_AF_SPI1);
    GPIO_PinAFConfig(GPIOA, GPIO_PinSource7, GPIO_AF_SPI1);

    // configure SPI1
    SPI_InitStruct.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
    SPI_InitStruct.SPI_Mode      = SPI_Mode_Master;
    SPI_InitStruct.SPI_DataSize  = SPI_DataSize_8b;
    SPI_InitStruct.SPI_CPOL      = SPI_CPOL_Low;
    SPI_InitStruct.SPI_CPHA      = SPI_CPHA_1Edge;
    SPI_InitStruct.SPI_NSS       = SPI_NSS_Soft | SPI_NSSInternalSoft_Set;
    SPI_InitStruct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_8;
    SPI_InitStruct.SPI_FirstBit  = SPI_FirstBit_MSB;
    SPI_Init(SPI1, &SPI_InitStruct);
    SPI_Cmd(SPI1, ENABLE);
}

uint8_t  SPIx_WriteRead(uint8_t byte)
{
    SPI1->DR = byte; // write data to be transmitted to the SPI data register
	while( !(SPI1->SR & SPI_I2S_FLAG_TXE) ); // wait until transmit complete
	while( !(SPI1->SR & SPI_I2S_FLAG_RXNE) ); // wait until receive complete
	while( SPI1->SR & SPI_I2S_FLAG_BSY ); // wait until SPI is not busy anymore
	return SPI1->DR; // return received data from SPI data register
}

/* Link function for GYRO peripheral */
void GYRO_IO_Init(void)
{
    /* Configure the Gyroscope Control pins
     * Enable CS GPIO clock and configure GPIO PIN for Gyroscope chip select */
    GPIO_InitTypeDef GPIO_InitStruct;
    GYRO_CS_GPIO_CLK_ENABLE();
    GPIO_InitStruct.GPIO_Pin = GYRO_CS_PIN;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStruct.GPIO_PuPd  = GPIO_PuPd_NOPULL;
    GPIO_Init(GYRO_CS_GPIO_PORT, &GPIO_InitStruct);

    /* Deselect : Chip Select high */
    GYRO_CS_HIGH();
    SPIx_Init();
}

void GYRO_IO_Write(uint8_t* pBuffer, uint8_t WriteAddr, uint16_t NumByteToWrite)
{
    if(NumByteToWrite > 0x01){
        WriteAddr |= (uint8_t) MULTIPLEBYTE_CMD;
    }
    GYRO_CS_LOW();
    SPIx_WriteRead(WriteAddr);
    while(NumByteToWrite >= 0x01)
    {
        SPIx_WriteRead(*pBuffer);
        NumByteToWrite--;
        pBuffer++;
    }
    GYRO_CS_HIGH();
}


void GYRO_IO_Read(uint8_t* pBuffer, uint8_t ReadAddr, uint16_t NumByteToRead)
{
    if(NumByteToRead > 0x01) {
      ReadAddr |= (uint8_t)(READWRITE_CMD | MULTIPLEBYTE_CMD);
    } else {
      ReadAddr |= (uint8_t)READWRITE_CMD;
    }
    GYRO_CS_LOW();
    SPIx_WriteRead(ReadAddr);
    while(NumByteToRead > 0x00)
    {
      *pBuffer = SPIx_WriteRead(DUMMY_BYTE);
      NumByteToRead--;
      pBuffer++;
    }
    GYRO_CS_HIGH();
}

void AUDIO_IO_Init(void)
{
    int i;
    GPIO_InitTypeDef GPIO_InitStruct;
    AUDIO_RESET_GPIO_CLK_ENABLE();
    GPIO_InitStruct.GPIO_Pin = AUDIO_RESET_PIN;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStruct.GPIO_PuPd  = GPIO_PuPd_NOPULL;
    GPIO_Init(AUDIO_RESET_GPIO, &GPIO_InitStruct);
    I2Cx_Init();
    CODEC_AUDIO_POWER_OFF();
    i = 10000;
    while(i--);
    CODEC_AUDIO_POWER_ON();
}
void AUDIO_IO_Write(uint8_t Addr, uint8_t Reg, uint8_t Value)
{
     I2Cx_WriteData(Addr, Reg, Value);
}
uint8_t AUDIO_IO_Read(uint8_t Addr, uint8_t Reg)
{
    return I2Cx_ReadData(Addr, Reg);    
}

/* Link function for COMPASS / ACCELERO peripheral */
void COMPASSACCELERO_IO_Init(void)
{
    GPIO_InitTypeDef GPIO_InitStruct;
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);
    GPIO_InitStruct.GPIO_Pin = ACCELERO_DRDY_PIN;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
    GPIO_Init(ACCELERO_DRDY_GPIO_PORT, &GPIO_InitStruct);
    I2Cx_Init();
}

void COMPASSACCELERO_IO_ITConfig(void)
{
}

void COMPASSACCELERO_IO_Write(uint16_t DeviceAddr, uint8_t RegisterAddr, uint8_t Value)
{
    I2Cx_WriteData(DeviceAddr, RegisterAddr, Value);
}
uint8_t COMPASSACCELERO_IO_Read(uint16_t DeviceAddr, uint8_t RegisterAddr)
{
    return I2Cx_ReadData(DeviceAddr, RegisterAddr);
}


uint32_t BSP_GetVersion(void)
{
  return __STM32F401_DISCO_BSP_VERSION;
}

void BSP_LED_Init(Led_TypeDef Led)
{
  GPIO_InitTypeDef  GPIO_InitStruct;
  
  /* Enable the GPIO_LED Clock */
  LEDx_GPIO_CLK_ENABLE(Led);

  /* Configure the GPIO_LED pin */
  GPIO_InitStruct.GPIO_Pin = GPIO_PIN[Led];
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
  
  GPIO_Init(GPIO_PORT[Led], &GPIO_InitStruct);
  GPIO_WriteBit(GPIO_PORT[Led], GPIO_PIN[Led], Bit_RESET); 
}

void BSP_LED_On(Led_TypeDef Led)
{
  GPIO_WriteBit(GPIO_PORT[Led], GPIO_PIN[Led], Bit_SET); 
}

void BSP_LED_Off(Led_TypeDef Led)
{
  GPIO_WriteBit(GPIO_PORT[Led], GPIO_PIN[Led], Bit_RESET); 
}

void BSP_LED_Toggle(Led_TypeDef Led)
{
  GPIO_ToggleBits(GPIO_PORT[Led], GPIO_PIN[Led]);
}


void BSP_PB_Init(Button_TypeDef Button, ButtonMode_TypeDef ButtonMode)
{
}

uint32_t BSP_PB_GetState(Button_TypeDef Button)
{
  return GPIO_ReadOutputDataBit(BUTTON_PORT[Button], BUTTON_PIN[Button]);
}
